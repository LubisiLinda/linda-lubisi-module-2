class MtnApps {
  String appName = '';
  String AppnamesWonByCategory = '';
  String developer = '';
  int yearWonApp = 0;

  @override
  String toString() {
    return 'App of the year winner was: $appName from the Sector $AppnamesWonByCategory developed by $developer in the year: $yearWonApp';
  }

  // a) an anonymous fucntion that changes the names of the apps in the "appNameList" into upperCases
  String upperCase(String name) {
    return appName.toUpperCase();
  }
}

void main() {
  //a list that contains all the names of the apps that won in the previous years 2012 to 2021
  var appNameList = [
    'FNB Banking App',
    'SnapScan',
    'LIVE Inspect',
    'WumDrop',
    'Domestly',
    'Shyft',
    'Khula ecosystem ',
    'Naked',
    'EasyEquities',
    'Ambani Africa App'
  ];
  // ignore: always_specify_types
  final List<String> developerList = [
    'FNB',
    'Kobus Ehlers',
    'Live Insert',
    'Simon Hartley and Roy Borole',
    'Berno Potgieter & Thatoyam Marumo',
    'Standard Bank',
    'AgriTech StartUp',
    'Sumarie Greybe, Ernest North and Alex Thomson',
    'First World Trader',
    'Mukundi Lambani'
  ];

  var categoryList = [
    'Best iOS App (consumer),Best BlackBerry app, Best Android app (consumer),Best Overall App',
    'Best HTML 5 app,Best Overall App',
    'Kobus Ehlers',
    'Best Android App (Enterprise),Best Overall App',
    'Best Enterprise App, Best Overall App',
    'Best Consumer App category, Best Overall App',
    'Best Financial Solution, Best Overall App',
    'Best Agriculture Solution, Best Overall App',
    'Best financial solution, Best app accolades, Best Overall App',
    'Best Consumer Solution, Best Overall App',
    'First World Trader',
    'Best Gaming Solution, Best Educational, Best South African Solution, Best Overall App'
  ];

  int n = 0;
  while (n < 10) {
    final MtnApps object = MtnApps();
    object.appName = appNameList[n];
    object.AppnamesWonByCategory = categoryList[n];
    object.yearWonApp = 2012 + n;
    print(object.upperCase(appNameList[n]));

    print(object);
    n++;
  }
}
