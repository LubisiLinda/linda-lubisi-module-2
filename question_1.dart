//Question: Write a basic program that stores and then prints the following data: Your name, favorite app, and city
void main() {
  const String name = 'Linda';
  const String favouriteApp = 'Namola';
  const String city = 'Johannesburg';

  print(
      'My name is $name.\nMy favourite App is $favouriteApp.\nMy city is in $city.');
}
