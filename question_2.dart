void main() {
  final appArray = [
    'FNB Banking App',
    'SnapScan',
    'LIVE Inspect',
    'WumDrop',
    'Domestly',
    'Shyft',
    'Khula ecosystem ',
    'Naked',
    'EasyEquities',
    'Ambani Africa App'
  ];

  // a)array of all the winning apps names in alphabetical order
  print(appArray..sort());

  //b)the winning app of 2017 and 2018.

  print('Winning App of 2017 was ' +
      appArray[5] +
      'and of 2018 was   ' +
      appArray[6]);

// c) The total number of apps from the array.
  print(appArray.length);
}
